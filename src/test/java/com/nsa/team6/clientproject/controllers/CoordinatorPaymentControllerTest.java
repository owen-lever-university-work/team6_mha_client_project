package com.nsa.team6.clientproject.controllers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext
@AutoConfigureMockMvc
public class CoordinatorPaymentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldReturnPage() throws Exception {
        mockMvc.perform(get("/coordinator/payment"))
                .andExpect(status().isOk())
                .andExpect(view().name("coordinatorPayForm.html"))
                .andExpect(content().string(containsString("Make a payment")))
                .andExpect(content().string(containsString("Transfer tokens to a volunteer")));
    }
}
