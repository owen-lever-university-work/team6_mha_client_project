package com.nsa.team6.clientproject.repositories;

import com.nsa.team6.clientproject.domain.Account;
import com.nsa.team6.clientproject.domain.Coordinator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class CoordinatorRepoTest {

    @Autowired
    private JdbcCoordinatorRepo jdbcCoordinatorRepo;

    @Test
    public void addCoordinatorAccount() throws Exception{
        int amount = ((List<Coordinator>)jdbcCoordinatorRepo.findAll()).size();
        String type = "Coordinator";
        int balance = 0;
        String name = "Test Account";

        String associatedCharity = "Test charity";
        Account account = new Account(type, balance, name);
        Coordinator coordinator = new Coordinator(associatedCharity, account);
        jdbcCoordinatorRepo.addNewCoordinator(coordinator);
        int newAmount = ((List<Coordinator>)jdbcCoordinatorRepo.findAll()).size();
        assertTrue(newAmount > amount);


    }
}
