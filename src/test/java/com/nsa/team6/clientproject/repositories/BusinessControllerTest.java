package com.nsa.team6.clientproject.repositories;

import com.nsa.team6.clientproject.controllers.AdminBusinessAccountController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import static org.assertj.core.api.Assertions.assertThat;



@RunWith(SpringRunner.class)
@SpringBootTest
public class BusinessControllerTest {

    @Autowired
    public AdminBusinessAccountController adminBusinessAccountController;

    @Test
    public void controllerLoads() throws Exception {
        assertThat(adminBusinessAccountController).isNotNull();
    }

}
