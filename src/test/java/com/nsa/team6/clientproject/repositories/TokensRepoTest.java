package com.nsa.team6.clientproject.repositories;

import com.nsa.team6.clientproject.domain.Token;
import com.nsa.team6.clientproject.domain.Transaction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class TokensRepoTest {

    @Autowired
    private JdbcTokenRepo jdbcTokenRepo;

    @Test
    public void shouldFindAllTokens(){
        assertThat(jdbcTokenRepo.findAll()).isNotNull();
    }

    @Test
    public void shouldAddTokensToAccount(){
        Iterable<Token> iterableList = jdbcTokenRepo.findAll();
        int numTokens = 0;
        for(Token t : iterableList){
            numTokens++;
        }

        jdbcTokenRepo.addTokensToAccount(2, "Dan", 2);

        Iterable<Token> iterableList2 = jdbcTokenRepo.findAll();
        ArrayList<Token> tokenList = new ArrayList<>();
        for(Token t : iterableList2){
            tokenList.add(t);
        }
        assertThat(numTokens + 2).isEqualTo(tokenList.size());
    }
}
