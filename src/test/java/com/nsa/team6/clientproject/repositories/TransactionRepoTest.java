package com.nsa.team6.clientproject.repositories;

import com.nsa.team6.clientproject.domain.TokenLedger;
import com.nsa.team6.clientproject.domain.Transaction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.ArrayList;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class TransactionRepoTest {

    @Autowired
    private JdbcTransactionRepo jdbcTransactionRepo;

    @Autowired
    private JdbcTokenLedgerRepo jdbcTokenLedgerRepo;

    @Autowired
    private JdbcTokenRepo jdbcTokenRepo;

    /**
     * Checks the number of transactions in the db
     */
    @Test
    public void shouldFindAllTransactions(){
        assertThat(jdbcTransactionRepo.findAll()).isNotNull();
    }

    /**
     * Checks that the number of transactions in the db increases by one once the sp has been called
     */
    @Test
    public void shouldCreatePaymentTransaction(){
        jdbcTokenRepo.addTokensToAccount(2, "Susan", 2);

        Iterable<Transaction> iterableList = jdbcTransactionRepo.findAll();
        int numTransactions = 0;
        for(Transaction t : iterableList){
            numTransactions++;
        }

        Iterable<TokenLedger> iterableTLList = jdbcTokenLedgerRepo.findAll();
        int numTL = 0;
        for(TokenLedger t : iterableTLList){
            numTL++;
        }

        jdbcTransactionRepo.paymentTransaction(2, "Clive", 1, 2);

        Iterable<Transaction> iterableList2 = jdbcTransactionRepo.findAll();
        int numTransactionsAfterCall = 0;
        for(Transaction t : iterableList2){
            numTransactionsAfterCall ++;
        }

        Iterable<TokenLedger> iterableTL2 = jdbcTokenLedgerRepo.findAll();
        int numTLAfterCall = 0;
        for(TokenLedger t : iterableTL2){
            numTLAfterCall ++;
        }

        //assertThat(numTransactions + 1).isEqualTo(numTransactionsAfterCall);
        //assertThat(numTL + 4).isEqualTo(numTLAfterCall);
    }
}
