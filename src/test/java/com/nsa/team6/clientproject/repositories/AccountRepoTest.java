package com.nsa.team6.clientproject.repositories;

import com.nsa.team6.clientproject.domain.Account;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import static org.junit.Assert.assertNotEquals;


import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.Assert.assertNotEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class AccountRepoTest {

    @Autowired
    private JdbcAccountRepo jdbcAccountRepo;

    @Test
    public void shouldFindAllAccounts(){
        assertThat(jdbcAccountRepo.findAll()).isNotNull();
    }

    @Test
    public void shouldFindAccountsByType(){
        Iterable<Account> iterableList = jdbcAccountRepo.findByAccountType("Coordinator");
        int count = 0;
        for(Account a : iterableList){
            count++;
        }
        assertNotEquals(0, count);
    }
}
