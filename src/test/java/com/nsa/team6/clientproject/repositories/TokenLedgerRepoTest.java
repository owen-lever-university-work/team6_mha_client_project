package com.nsa.team6.clientproject.repositories;

import com.nsa.team6.clientproject.domain.Account;
import com.nsa.team6.clientproject.domain.TokenLedger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class TokenLedgerRepoTest {

    @Autowired
    private JdbcTokenLedgerRepo jdbcTokenLedgerRepo;

    @Test
    public void getJdbcTokenLedgerRepo() {
        assertThat(jdbcTokenLedgerRepo.findAll()).isNotNull();
    }
}
