DROP TABLE IF EXISTS tokens;

DROP TABLE IF EXISTS tokenledgers;

DROP TABLE IF EXISTS transactions;

DROP TABLE IF EXISTS accounts;

DELETE FROM accounts;

DELETE FROM tokens;

DELETE FROM tokenledger;

DELETE FROM transactions;

DROP TRIGGER IF EXISTS `tokenledgers_AFTER_INSERT`;

DROP procedure IF EXISTS `create_tokens`;

DROP procedure IF EXISTS `create_transaction`;

CREATE TABLE accounts (
  id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
  account_type VARCHAR(20) NOT NULL,
  balance int NOT NULL,
  account_name VARCHAR(50)
);

CREATE TABLE tokens (
  id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
  token_number VARCHAR(15) NOT NULL,
  created_on DATETIME NOT NULL,
  account_id int NOT NULL,
  FOREIGN KEY(account_id) REFERENCES accounts(id)
);

CREATE TABLE transactions (
  id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
  created_on datetime NOT NULL,
  transaction_type VARCHAR(20) NOT NULL,
  created_by VARCHAR(50) NOT NULL,
  memo VARCHAR(255)
);

CREATE TABLE tokenledgers (
  id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
  direction VARCHAR(6) NOT NULL,
  created_on DATETIME NOT NULL,
  account_id int NOT NULL,
  transaction_id int NOT NULL,
  token_id int NOT NULL,
  FOREIGN KEY(account_id) REFERENCES accounts(id),
  FOREIGN KEY(transaction_id) REFERENCES transactions(id),
  FOREIGN KEY(token_id) references tokens(id)
);

DELIMITER //
CREATE TRIGGER tokenledgers_AFTER_INSERT
AFTER INSERT ON tokenledgers
FOR EACH ROW
BEGIN
IF new.direction = 'Credit'
THEN
UPDATE accounts SET balance = balance + 1 WHERE new.account_id = accounts.id;
END IF;
IF new.direction = 'Debit'
THEN
UPDATE accounts SET balance = balance - 1 WHERE new.account_id = accounts.id;
END IF;
END//
DELIMITER ;

DELIMITER $$
USE `client_project`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `create_tokens`(numberOfTokens int, whoCreated varchar(30), ownerAccountId int)
BEGIN
	DECLARE x INT;
	SET x = 0;

	INSERT INTO transactions(created_on, transaction_type, created_by, memo) VALUES(current_timestamp, 'Create tokens', whoCreated, CONCAT('Created ', CAST(numberOfTokens AS CHAR(4)), ' tokens'));
	SET @transaction_id = (SELECT LAST_INSERT_ID());
	WHILE( x < numberOfTokens) DO
		INSERT INTO tokens(token_number, created_on, account_id) VALUES (CONVERT(floor(1000000 + (rand() * 89999999)), CHAR(8)), current_timestamp, ownerAccountId);
		SET @token_var_id = (SELECT LAST_INSERT_ID());
		INSERT INTO tokenledgers(direction, created_on, account_id, transaction_id, token_id)VALUES('Credit', current_timestamp, ownerAccountId, @transaction_id, @token_var_id);
		SET x = x + 1;
	END WHILE;
	 -- Account balances are updated via tokenledgers_AFTER_INSERT trigger
END$$
DELIMITER ;

/**
Creates a transaction to pass tokens between two accounts
 */
DELIMITER $$
USE `client_project`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `payment_transaction`(numberOfTokens int, whoCreated varchar(30), creditAccountId int, debitAccountId int)
BEGIN
	DECLARE x INT;
  DECLARE token_var_id INT;
	SET x = 0;

    START TRANSACTION;
	INSERT INTO transactions(created_on, transaction_type, created_by, memo) VALUES(current_timestamp, 'Pay tokens', whoCreated, CONCAT('Transfered ', CAST(numberOfTokens AS CHAR(4)), ' tokens'));

    SET @transaction_id = (SELECT LAST_INSERT_ID());
	WHILE( x < numberOfTokens) DO
			SET token_var_id = (SELECT id FROM tokens WHERE account_id = debitAccountId LIMIT 1);
			UPDATE tokens SET account_id = creditAccountId WHERE id = @token_var_id;
			INSERT INTO tokenledgers(direction, created_on, account_id, transaction_id, token_id)VALUES('Credit', current_timestamp, creditAccountId, @transaction_id, token_var_id);
			INSERT INTO tokenledgers(direction, created_on, account_id, transaction_id, token_id)VALUES('Debit', current_timestamp, debitAccountId, @transaction_id, token_var_id);
            -- Account balance updated via trigger
			SET x = x + 1;
	END WHILE;
    COMMIT;
END$$
