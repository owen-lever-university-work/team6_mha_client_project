DELETE FROM coordinators;

DELETE FROM accounts;

DELETE FROM tokens;

DELETE FROM tokenledger;

DELETE FROM transactions;

DELETE FROM volunteers;