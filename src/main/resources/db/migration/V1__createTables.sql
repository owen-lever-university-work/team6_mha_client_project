CREATE TABLE accounts (
  id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
  account_type VARCHAR(20) NOT NULL,
  balance int NOT NULL,
  account_name VARCHAR(50)
);

CREATE TABLE tokens (
  id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
  token_number VARCHAR(15) NOT NULL,
  created_on DATETIME NOT NULL,
  account_id int NOT NULL,
  FOREIGN KEY(account_id) REFERENCES accounts(id)
);

CREATE TABLE transactions (
  id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
  created_on datetime NOT NULL,
  transaction_type VARCHAR(20) NOT NULL,
  created_by VARCHAR(50) NOT NULL,
  memo VARCHAR(255)
);

CREATE TABLE tokenledgers (
  id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
  direction VARCHAR(6) NOT NULL,
  created_on DATETIME NOT NULL,
  account_id int NOT NULL,
  transaction_id int NOT NULL,
  token_id int NOT NULL,
  FOREIGN KEY(account_id) REFERENCES accounts(id),
  FOREIGN KEY(transaction_id) REFERENCES transactions(id),
  FOREIGN KEY(token_id) REFERENCES tokens(id)
);

CREATE TABLE volunteers (
  id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
  registration_date DATETIME NOT NULL,
  volunteer_name VARCHAR(20) NOT NULL,
  address VARCHAR(50) NOT NULL,
  postcode VARCHAR(10) NOT NULL,
  phone_number VARCHAR(11) NOT NULL,
  emergency_number VARCHAR(11) NOT NULL,
  email VARCHAR(50) NOT NULL,
  tenancy BOOLEAN NOT NULL,
  volunteered BOOLEAN NOT NULL,
  disabilities BOOLEAN NOT NULL,
  medical_conditions BOOLEAN NOT NULL,
  allergies VARCHAR(255) NOT NULL,
  account_id int NOT NULL,
  FOREIGN KEY(account_id) REFERENCES accounts(id)
);

CREATE TABLE coordinators (
  id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
  associated_charity VARCHAR(20) NOT NULL,
  account_id int NOT NULL,
  FOREIGN KEY(account_id) REFERENCES accounts(id)
);