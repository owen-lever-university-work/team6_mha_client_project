
/**
Updates the balance of an account according to whether it is credited or debited from the account after a tokenleger is inserted
 */
DROP TRIGGER IF EXISTS `tokenledgers_AFTER_INSERT`;

DELIMITER //
CREATE TRIGGER tokenledgers_AFTER_INSERT
AFTER INSERT ON tokenledgers
FOR EACH ROW
BEGIN
IF new.direction = 'Credit'
THEN
UPDATE accounts SET balance = balance + 1 WHERE new.account_id = accounts.id;
END IF;
IF new.direction = 'Debit'
THEN
UPDATE accounts SET balance = balance - 1 WHERE new.account_id = accounts.id;
END IF;
END//
DELIMITER ;