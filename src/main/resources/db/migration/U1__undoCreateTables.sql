DROP TABLE IF EXISTS tokens;

DROP TABLE IF EXISTS tokenledgers;

DROP TABLE IF EXISTS transactions;

DROP TABLE IF EXISTS accounts;

DROP TABLE IF EXISTS volunteers;

DROP TABLE if EXISTS coordinators;
