DELIMITER $$
CREATE FUNCTION `retrieve_token_volunteer`(volunteerId int) RETURNS varchar(255) CHARSET latin1
READS SQL DATA
BEGIN
	DECLARE token VARCHAR(255);
	DECLARE x INT;
	SET x = 0;

    IF (SELECT COUNT(*) FROM tokens t INNER JOIN accounts a ON t.account_id = a.id INNER JOIN volunteers v ON a.id = v.account_id WHERE v.id = volunteerId) > 0 THEN
		SET token = (SELECT t.id FROM tokens t
		INNER JOIN accounts a
		ON t.account_id = a.id
		INNER JOIN volunteers v
		ON a.id = v.account_id
		WHERE v.id = volunteerId LIMIT 1);
    ELSE
		SET token = "Error: Not enough tokens in account";
	END IF;

RETURN token;
END$$