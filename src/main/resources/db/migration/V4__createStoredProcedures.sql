DROP procedure IF EXISTS `create_tokens`;

DROP procedure IF EXISTS `create_transaction`;

DROP procedure IF EXISTS `create_volunteer_accounts`;

DROP procedure IF EXISTS `create_business_account`;

DROP procedure IF EXISTS `create_coordinator_accounts`;

/**
Generates tokens and adds tokens to users account
 */

DELIMITER $$
CREATE PROCEDURE `create_tokens`(numberOfTokens int, whoCreated varchar(30), ownerAccountId int)
BEGIN
	DECLARE x INT;
	DECLARE Success Boolean;
	SET x = 0;
	SET Success = TRUE;

  START TRANSACTION;
	INSERT INTO transactions(created_on, transaction_type, created_by, memo) VALUES(current_timestamp, 'Create tokens', whoCreated, CONCAT('Created ', CAST(numberOfTokens AS CHAR(4)), ' tokens'));
	SET @transaction_id = (SELECT LAST_INSERT_ID());

    IF numberOfTokens > 0 THEN
	WHILE( x < numberOfTokens) DO
		INSERT INTO tokens(token_number, created_on, account_id) VALUES (CONVERT(floor(1000000 + (rand() * 89999999)), CHAR(8)), current_timestamp, ownerAccountId);
		SET @token_var_id = (SELECT LAST_INSERT_ID());
		INSERT INTO tokenLedgers(direction, created_on, account_id, transaction_id, token_id)VALUES('Credit', current_timestamp, ownerAccountId, @transaction_id, @token_var_id);
		SET x = x + 1;
	END WHILE;
    ELSE
    SET Success = FALSE;
    END IF;

    IF Success = FALSE THEN
		ROLLBACK;
		SELECT "Transaction has been rolled back because the number of tokens is " as Message;
	ELSE
		COMMIT;
		SELECT "Transaction has been commited. Tokens have been transfered" as Message;
	END IF;
END$$

DELIMITER ;

/**
Creates a transaction to pass tokens between two accounts
 */
DROP procedure IF EXISTS `payment_transaction`;

DELIMITER $$
CREATE PROCEDURE `payment_transaction`(numberOfTokens int, whoCreated varchar(30), creditAccountId int, debitAccountId int)
BEGIN
	DECLARE x INT;
	DECLARE Success Boolean;
	SET x = 0;
	SET Success = TRUE;

  START TRANSACTION;
	INSERT INTO transactions(created_on, transaction_type, created_by, memo) VALUES(current_timestamp, 'Pay tokens', whoCreated, CONCAT('Transfered ', CAST(numberOfTokens AS CHAR(4)), ' tokens'));
    SET @transaction_id = (SELECT LAST_INSERT_ID());

    IF (SELECT COUNT(*) FROM tokens WHERE account_id = debitAccountId) > numberOfTokens THEN
		WHILE( x < numberOfTokens) DO
				SELECT id INTO @var_token_id FROM tokens WHERE account_id = debitAccountId ORDER BY id LIMIT 1;
				IF @var_token_id IS NOT NULL
				THEN
				UPDATE tokens SET account_id = creditAccountId WHERE id = @var_token_id;
				INSERT INTO tokenledgers(direction, created_on, account_id, transaction_id, token_id)VALUES('Credit', current_timestamp, creditAccountId, @transaction_id, @var_token_id);
				INSERT INTO tokenledgers(direction, created_on, account_id, transaction_id, token_id)VALUES('Debit', current_timestamp, debitAccountId, @transaction_id, @var_token_id);
				-- Account balance updated via trigger
				END IF;
				SET x = x + 1;
		END WHILE;
    ELSE
		SET Success = FALSE;
	END IF;

	IF Success = FALSE THEN
		ROLLBACK;
		SELECT "Transaction has been rolled back because there are not enough tokens in the debit account" as Message;
	ELSE
		COMMIT;
		SELECT "Transaction has been commited. Tokens have been transfered" as Message;
	END IF;
END$$

DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `create_volunteer_accounts` (registration_date DATETIME, volunteer_name VARCHAR(20), address VARCHAR(50), postcode VARCHAR(10), phone_number VARCHAR(11), emergency_number VARCHAR(11), email VARCHAR(50), tenancy BOOLEAN, volunteered BOOLEAN, disabilities BOOLEAN, medical_conditions BOOLEAN, allergies VARCHAR(255))
BEGIN
INSERT INTO accounts (account_type, balance, account_name) VALUES ('Volunteer', 0, volunteer_name);
SET @account_id=(SELECT LAST_INSERT_ID());
INSERT INTO volunteers (registration_date, volunteer_name, address, postcode, phone_number, emergency_number, email, tenancy, volunteered, disabilities, medical_conditions, allergies, account_id) VALUES (registration_date, volunteer_name, address, postcode, phone_number, emergency_number, email, tenancy, volunteered, disabilities, medical_conditions, allergies, account_id);
END$$

DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `create_business_account`( business_name VARCHAR(50), address VARCHAR(45), postcode VARCHAR(8), phone_number varchar(11), email VARCHAR(30))
BEGIN
INSERT INTO accounts(account_type, balance,account_name) VALUES ("Business", 0, business_name);
SET @account_id=(SELECT LAST_INSERT_ID());
INSERT INTO business(business_name,  address, postcode, phone_number, email, account_id) VALUES(business_name,  address, postcode, phone_number, email, @account_id);
END$$
DELIMITER ;

/**
Creates and saves the coordinator account and the account
 */
DELIMITER $$
CREATE PROCEDURE `create_coordinator_accounts`(balance int, account_name varchar(50), associated_charity varchar(50))
BEGIN
	INSERT INTO accounts (account_type, balance, account_name) VALUES ('Coordinator', balance, account_name);
    SET @account_id = (SELECT LAST_INSERT_ID());
    INSERT INTO coordinators (associated_charity, account_id) VALUES (associated_charity, @account_id);
END$$

DELIMITER ;
