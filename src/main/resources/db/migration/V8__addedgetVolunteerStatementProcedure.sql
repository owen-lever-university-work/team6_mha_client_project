DROP procedure IF EXISTS `get_volunteer_statement`;

DELIMITER $$
CREATE PROCEDURE `get_volunteer_statement`(volunteerId int)
BEGIN
#https://stackoverflow.com/a/29130115 (Retrieved 27/11/18) Date loop
DECLARE StartDate DATE;
DECLARE EndDate DATE;
DECLARE AccountId int;
DECLARE tlLoopCounter int DEFAULT 0;

SET StartDate = '2018-11-01';
SET EndDate = DATE(NOW());
SET @CurrentDate = StartDate;
SET AccountId = (SELECT account_id FROM volunteers WHERE id = volunteerId);

CREATE temporary table statement_table (id int NOT NULL AUTO_INCREMENT PRIMARY KEY, date_of_transaction DATE, tokens_paid int, balance int);

WHILE(@CurrentDate < EndDate) DO

    SET @TokensPaid = (
    SELECT COUNT(tl.id) FROM tokenledgers tl
    INNER JOIN accounts a ON a.id = tl.account_id
    WHERE a.id = AccountId
    AND DATE(tl.created_on) = @CurrentDate
    AND tl.id = volunteerId
    AND tl.direction = 'Credit'
    GROUP BY DATE(tl.created_on));

	SET @IncrementingDate = StartDate;
    SET @AccumulatedBalance = 0;
    WHILE(@IncrementingDate < @CurrentDate) DO

		SET @numTokenLedgersForDay = (SELECT COUNT(*) FROM tokenledgers WHERE DATE(created_on) = @IncrementingDate AND account_id = AccountId);
		SET tlLoopCounter = 0;

		WHILE tlLoopCounter < @numTokenLedgersForDay DO
			IF (SELECT COUNT(*) FROM tokenledgers WHERE DATE(created_on) = @IncrementingDate AND account_id = AccountId AND direction = 'Credit' LIMIT tlLoopCounter, 1) = 1 THEN
				SET @AccumulatedBalance = @AccumulatedBalance + 1;
				SET tlLoopCounter = tlLoopCounter + 1;
			ELSEIF (SELECT COUNT(*) FROM tokenledgers WHERE DATE(created_on) = @IncrementingDate AND account_id = AccountId AND direction = 'Debit' LIMIT tlLoopCounter, 1) = 1 THEN
				SET @AccumulatedBalance = @AccumulatedBalance - 1;
				SET tlLoopCounter = tlLoopCounter + 1;
			END IF;
		END WHILE;
		SET @IncrementingDate = DATE_ADD(@IncrementingDate, INTERVAL 1 DAY);

	END WHILE;
  INSERT INTO statement_table(date_of_transaction, tokens_paid, balance) VALUES(@CurrentDate, IFNULL(@TokensPaid, 0), IFNULL(@AccumulatedBalance,0));
	SET @CurrentDate = DATE_ADD(@CurrentDate, INTERVAL 1 DAY);

 END WHILE;

 SELECT * FROM statement_table ORDER BY date_of_transaction DESC;
 DROP TABLE IF EXISTS statement_table;
END$$

DELIMITER ;