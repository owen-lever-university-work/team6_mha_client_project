
DROP function IF EXISTS `count_account_types`;

DELIMITER $$

CREATE FUNCTION `count_account_types`(accountType VARCHAR(20)) RETURNS varchar(255) CHARSET latin1
READS SQL DATA
BEGIN
DECLARE entityCount VARCHAR(255);

	IF (SELECT COUNT(*) FROM accounts WHERE account_type = accountType) <> 0 THEN
		SET entityCount = (
        SELECT CONCAT ('There are ', COUNT(*), ' ', LOWER(accountType), ' accounts') FROM accounts
        WHERE account_type = accountType);
	ELSE
		SET entityCount = 'Error: No account of that type could be found';
	END IF;
RETURN entityCount;
END$$

DELIMITER ;