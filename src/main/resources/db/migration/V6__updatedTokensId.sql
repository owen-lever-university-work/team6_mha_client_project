DROP TABLE IF EXISTS tokenledgers;

DROP TABLE IF EXISTS tokens;

#Changed the PK now token_number
CREATE TABLE tokens (
  id int NOT NULL PRIMARY KEY UNIQUE,
  created_on DATETIME NOT NULL,
  account_id int NOT NULL,
  FOREIGN KEY(account_id) REFERENCES accounts(id)
);

CREATE TABLE tokenledgers (
  id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
  direction VARCHAR(6) NOT NULL,
  created_on DATETIME NOT NULL,
  account_id int NOT NULL,
  transaction_id int NOT NULL,
  token_id int NOT NULL,
  FOREIGN KEY(account_id) REFERENCES accounts(id),
  FOREIGN KEY(transaction_id) REFERENCES transactions(id),
  FOREIGN KEY(token_id) REFERENCES tokens(id)
);

#Changed the token var to take into account the new id system
DROP procedure IF EXISTS `create_tokens`;

DELIMITER $$
CREATE PROCEDURE `create_tokens`(numberOfTokens int, whoCreated varchar(30), ownerAccountId int)
BEGIN
	DECLARE x INT;
	DECLARE Success Boolean;
	SET x = 0;
	SET Success = TRUE;

  START TRANSACTION;
	INSERT INTO transactions(created_on, transaction_type, created_by, memo) VALUES(current_timestamp, 'Create tokens', whoCreated, CONCAT('Created ', CAST(numberOfTokens AS CHAR(4)), ' tokens'));
	SET @transaction_id = (SELECT LAST_INSERT_ID());

    IF numberOfTokens > 0 THEN
	WHILE( x < numberOfTokens) DO
		INSERT INTO tokens(id, created_on, account_id) VALUES (CONVERT(floor(1000000 + (rand() * 89999999)), CHAR(8)), current_timestamp, ownerAccountId);
		SET @token_var_id = (SELECT id FROM tokens WHERE account_id = ownerAccountId ORDER BY created_on DESC LIMIT 1);
		INSERT INTO tokenledgers(direction, created_on, account_id, transaction_id, token_id)VALUES('Credit', current_timestamp, ownerAccountId, @transaction_id, @token_var_id);
		SET x = x + 1;
	END WHILE;
    ELSE
    SET Success = FALSE;
    END IF;

    IF Success = FALSE THEN
		ROLLBACK;
		SELECT "Transaction has been rolled back because the number of tokens is " as Message;
	ELSE
		COMMIT;
		SELECT "Transaction has been commited. Tokens have been transfered" as Message;
	END IF;
END$$
DELIMITER ;

#Changed the token var to take into account the new id system
DROP procedure IF EXISTS `payment_transaction`;

DELIMITER $$
CREATE PROCEDURE `payment_transaction`(numberOfTokens int, whoCreated varchar(30), creditAccountId int, debitAccountId int)
BEGIN
	DECLARE x INT;
	DECLARE Success Boolean;
	SET x = 0;
	SET Success = TRUE;

    START TRANSACTION;
	INSERT INTO transactions(created_on, transaction_type, created_by, memo) VALUES(current_timestamp, 'Pay tokens', whoCreated, CONCAT('Transfered ', CAST(numberOfTokens AS CHAR(4)), ' tokens'));
    SET @transaction_id = (SELECT LAST_INSERT_ID());

    IF (SELECT COUNT(*) FROM tokens WHERE account_id = debitAccountId) > numberOfTokens THEN
		WHILE( x < numberOfTokens) DO
				#SELECT id INTO @var_token_id FROM tokens WHERE account_id = debitAccountId ORDER BY id LIMIT 1;
                SET @token_var_id = (SELECT id FROM tokens WHERE account_id = debitAccountId ORDER BY id LIMIT 1);
				IF @token_var_id IS NOT NULL
				THEN
				UPDATE tokens SET account_id = creditAccountId WHERE id = @var_token_id;
				INSERT INTO tokenledgers(direction, created_on, account_id, transaction_id, token_id)VALUES('Credit', current_timestamp, creditAccountId, @transaction_id, @token_var_id);
				INSERT INTO tokenledgers(direction, created_on, account_id, transaction_id, token_id)VALUES('Debit', current_timestamp, debitAccountId, @transaction_id, @token_var_id);
				-- Account balance updated via trigger
				END IF;
				SET x = x + 1;
		END WHILE;
    ELSE
		SET Success = FALSE;
	END IF;

	IF Success = FALSE THEN
		ROLLBACK;
		SELECT "Transaction has been rolled back because there are not enough tokens in the debit account" as Message;
	ELSE
		COMMIT;
		SELECT "Transaction has been commited. Tokens have been transfered" as Message;
	END IF;
END$$

DELIMITER ;