

INSERT INTO accounts(account_type, balance, account_name) VALUES ('Admin', 0, 'Admin account');
INSERT INTO accounts(account_type, balance, account_name) VALUES ('Coordinator', 0, 'Jake');
INSERT INTO accounts(account_type, balance, account_name) VALUES ('Coordinator', 0, 'Susan');
INSERT INTO accounts(account_type, balance, account_name) VALUES ('Volunteer', 0, 'Jenny');
INSERT INTO accounts(account_type, balance, account_name) VALUES ('Volunteer', 0, 'Sasha');
INSERT INTO accounts(account_type, balance, account_name) VALUES ('Volunteer', 0, 'Mark');
INSERT INTO accounts(account_type, balance, account_name) VALUES ('Volunteer', 0, 'Steve');
INSERT INTO accounts(account_type, balance, account_name) VALUES ('Business', 0, 'Tesco');
INSERT INTO accounts(account_type, balance, account_name) VALUES ('Business', 0, 'Cinema');

INSERT INTO tokens(token_number, created_on, account_id) VALUES (37294836, now(), 2);
INSERT INTO tokens(token_number, created_on, account_id) VALUES (37275836, now(), 2);
INSERT INTO tokens(token_number, created_on, account_id) VALUES (93294836, now(), 2);

UPDATE accounts SET balance = balance + 3 WHERE id = 2;

INSERT INTO tokens(token_number, created_on, account_id) VALUES (37294636, now(), 3);
INSERT INTO tokens(token_number, created_on, account_id) VALUES (37275826, now(), 3);
INSERT INTO tokens(token_number, created_on, account_id) VALUES (92994836, now(), 3);

UPDATE accounts SET balance = 3 WHERE id = 3;

INSERT INTO tokens(token_number, created_on, account_id) VALUES (37294636, now(), 4);
INSERT INTO tokens(token_number, created_on, account_id) VALUES (37275826, now(), 4);
INSERT INTO tokens(token_number, created_on, account_id) VALUES (92994836, now(), 4);

UPDATE accounts SET balance = 3 WHERE id = 4;

INSERT INTO tokens(token_number, created_on, account_id) VALUES (37294636, now(), 5);
INSERT INTO tokens(token_number, created_on, account_id) VALUES (37275826, now(), 5);

UPDATE accounts SET balance = 3 WHERE id = 5;
