DROP procedure IF EXISTS `create_tokens`;

DROP procedure IF EXISTS `create_transaction`;

DROP procedure IF EXISTS `create_volunteer_accounts`;

DROP procedure IF EXISTS `create_business_account`;

DROP procedure IF EXISTS `create_coordinator_accounts`;
