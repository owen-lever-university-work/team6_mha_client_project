
INSERT INTO accounts(account_type, balance, account_name) VALUES ('Admin', 0, 'Karen');
INSERT INTO accounts(account_type, balance, account_name) VALUES ('Admin', 0, 'Dan');
INSERT INTO accounts(account_type, balance, account_name) VALUES ('Coordinator', 0, 'Jake');
INSERT INTO accounts(account_type, balance, account_name) VALUES ('Coordinator', 0, 'Susan');
INSERT INTO accounts(account_type, balance, account_name) VALUES ('Volunteer', 0, 'Jenny');
INSERT INTO accounts(account_type, balance, account_name) VALUES ('Volunteer', 0, 'Sally');
INSERT INTO accounts(account_type, balance, account_name) VALUES ('Volunteer', 0, 'Paul');
INSERT INTO accounts(account_type, balance, account_name) VALUES ('Volunteer', 0, 'Rob');
INSERT INTO accounts(account_type, balance, account_name) VALUES ('Business', 0, 'Tesco');
INSERT INTO accounts(account_type, balance, account_name) VALUES ('Business', 0, 'Cinema');

INSERT INTO tokens(token_number, created_on, account_id) VALUES (12345678, current_timestamp, 1);
INSERT INTO tokens(token_number, created_on, account_id) VALUES (12345687, current_timestamp, 1);