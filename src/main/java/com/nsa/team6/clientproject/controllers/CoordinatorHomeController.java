package com.nsa.team6.clientproject.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping(path = "/coordinator")
public class CoordinatorHomeController {
    static final Logger LOG = LoggerFactory.getLogger(com.nsa.team6.clientproject.controllers.AdminHomeController.class);

    @RequestMapping(path = "/home")
    public String adminHome() {
        LOG.debug("Handling GET request to /home.html");
        //Returns the home page
        return "coordinatorHome";
    }
}