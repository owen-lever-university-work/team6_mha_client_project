package com.nsa.team6.clientproject.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/admin")
public class AdminHomeController {
    static final Logger LOG = LoggerFactory.getLogger(AdminHomeController.class);

    @RequestMapping(path = "/adminHome")
    public String adminHome() {
        LOG.debug("Handling GET request to /adminHome");
        //Returns the home page
        return "adminHome";
    }
}
