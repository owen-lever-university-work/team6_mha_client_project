package com.nsa.team6.clientproject.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class VolunteerHomeController {

    static final Logger LOG = LoggerFactory.getLogger(VolunteerHomeController.class);

    @RequestMapping(path = "/volunteerHome")
    public String volunteerHome() {
        LOG.debug("Handling GET request to /volunteerHome");
        //Returns the home page
        return "volunteerHome";
    }

}

