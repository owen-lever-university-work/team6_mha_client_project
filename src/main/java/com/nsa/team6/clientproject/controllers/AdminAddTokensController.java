package com.nsa.team6.clientproject.controllers;

import com.nsa.team6.clientproject.domain.Account;
import com.nsa.team6.clientproject.forms.AddTokenForm;
import com.nsa.team6.clientproject.services.AccountService;
import com.nsa.team6.clientproject.services.TokenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Controller
@RequestMapping(path = "/admin")
public class AdminAddTokensController {

    static final Logger LOG = LoggerFactory.getLogger(AdminAddTokensController.class);

    private AccountService accountService;
    private TokenService tokenService;

    @Autowired
    public AdminAddTokensController(TokenService tService, AccountService aService){
        tokenService = tService;
        accountService = aService;
    }

    @RequestMapping(value = "/addTokens", method = RequestMethod.GET)
    public ModelAndView showForm(Model model) {
        model.addAttribute("tokenForm", new AddTokenForm());
        Iterable<Account> iterable = accountService.findByAccountType("Admin");
        List<Account> accounts = StreamSupport
                .stream(iterable.spliterator(), false)
                .collect(Collectors.toList());

        model.addAttribute("accountBalance", accounts.get(0).getBalance());
        return new ModelAndView("adminAddTokenForm");
    }

    @RequestMapping(value = "/addTokens", method = RequestMethod.POST)
    public String createTokensFromForm(Model model, @ModelAttribute("tokenForm") @Valid AddTokenForm addToken, BindingResult bindingResult) {
        //If there are errors with the inputted form info, then the form page will be returned again
        if (bindingResult.hasErrors()) {
            LOG.error(bindingResult.toString());
            LOG.error("Form has binding errors");
            return "adminAddTokenForm";
        }
        LOG.info("Form received");

        tokenService.addTokensToAccount(addToken.getNumberOfTokens(), addToken.getWhoCreated(), 1);

        return "adminSuccessAddedTokens";
    }
}
