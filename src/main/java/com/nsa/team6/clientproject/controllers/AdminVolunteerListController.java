package com.nsa.team6.clientproject.controllers;

import com.nsa.team6.clientproject.domain.Account;
import com.nsa.team6.clientproject.services.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping(path = "/admin")
public class AdminVolunteerListController {

    private AccountService accountService;

    @Autowired
    public AdminVolunteerListController(AccountService aService){
        accountService = aService;
    }


    @RequestMapping(value = "/volunteerList", method = RequestMethod.GET)
    public ModelAndView showForm(Model model) {
        List<Account> volunteerAccounts = (List<Account>) accountService.findByAccountType("Volunteer");
        model.addAttribute("accounts", volunteerAccounts);
        return new ModelAndView("adminVolunteerList");
    }
}
