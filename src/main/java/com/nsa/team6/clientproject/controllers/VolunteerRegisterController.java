package com.nsa.team6.clientproject.controllers;

import com.nsa.team6.clientproject.domain.Volunteer;
import com.nsa.team6.clientproject.repositories.VolunteerRegisterInterface;
import com.nsa.team6.clientproject.services.VolunteerRegisterService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.Map;
import java.util.logging.Logger;

@Controller
public class VolunteerRegisterController {


    private VolunteerRegisterService volunteerRegisterService;

    static final org.slf4j.Logger LOG = LoggerFactory.getLogger(VolunteerRegisterController.class);

    @Autowired
    public VolunteerRegisterController (VolunteerRegisterService aService) { volunteerRegisterService = aService; }



    //code that directs the user to the register form to register volunteers
    @RequestMapping(path = "/volunteerRegister", method = RequestMethod.GET)
    public String volunteerRegister(Model model) {
        LOG.debug("Handling GET request to /volunteerRegister");
        return "volunteerRegister";
    }

    @RequestMapping(path = "/volunteerRegister", method = RequestMethod.POST)
    public String volRegister(Volunteer volunteer, Model model) {
//        if (bindingResult.hasErrors()) {
//            System.out.println(bindingResult);
//        }
        System.out.println("Volunteer Registered!");
        LOG.debug(volunteer.toString());
        volunteerRegisterService.addVolunteer(volunteer);
        model.addAttribute("volunteer", volunteer);
        return "volunteerRegister";
//        model.addAttribute("ID", volService.getId());
//        model.addAttribute("Registration Date", volService.getRegistrationDate());
//        model.addAttribute("Name", volService.getVolunteerName());
//        model.addAttribute("Address", volService.getVolunteerAddress());
//        model.addAttribute("Postcode", volService.getVolunteerPostCode());
//        model.addAttribute("Phone Number", volService.getVolunteerPhoneNo());
//        model.addAttribute("Emergency Contact Number", volService.getEmergencyContactNo());
//        model.addAttribute("Email", volService.getVolunteerEmail());
//        model.addAttribute("MHA Tenancy", volService.getIsMHATenant());
//        model.addAttribute("Volunteering", volService.getIsVolunteer());
//        model.addAttribute("Disabilities", volService.getIsDisabilities());
//        model.addAttribute("Medical Conditions", volService.getIsMedicalConditions());
//        model.addAttribute("Allergies", volService.getVolunteerAllergies());
//        //volunteerRegisterInterface.findAll();
//        volunteerRegisterInterface.save(volService);
//        return new ModelAndView("redirect/volunteerRegister", model.asMap());
    }

}
