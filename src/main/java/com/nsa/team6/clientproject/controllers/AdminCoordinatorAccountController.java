package com.nsa.team6.clientproject.controllers;


import com.nsa.team6.clientproject.domain.Account;
import com.nsa.team6.clientproject.domain.Coordinator;
import com.nsa.team6.clientproject.repositories.AccountRepoInterface;
import com.nsa.team6.clientproject.repositories.CoordinatorRepoInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Controller
public class AdminCoordinatorAccountController {

    AccountRepoInterface ari;
    CoordinatorRepoInterface cri;

    @Autowired
    public AdminCoordinatorAccountController(AccountRepoInterface ari, CoordinatorRepoInterface cri){
        this.ari = ari;
        this.cri = cri;
    }


    @RequestMapping(path="/admin/coordinators", method = RequestMethod.GET)
    public String getAllCoordinatorAccounts(Model model){
        List<Coordinator> coordinators = new LinkedList<Coordinator>();
        for(Coordinator c: cri.findAll()){
            coordinators.add(c);
        }
        model.addAttribute("coordinators", coordinators);


        return "adminViewCoordinators";
    }



    @RequestMapping(path="/admin/add_coordinators", method  = RequestMethod.GET)
    public String getAddCoordinatorPage(){

        return "adminAddCoordinatorAccountForm";
    }





    @RequestMapping(path="/admin/add_coordinators", method  = RequestMethod.POST)
    public String addCoordinatorAccount(@RequestParam Map<String, String> requestParams, Model model){
        if(requestParams.containsKey("firstName") &&
                requestParams.get("firstName").length()>0 &&
                requestParams.containsKey("associatedCharity") &&
                requestParams.get("associatedCharity").length()>0){

            String name = requestParams.get("firstName");
            if(requestParams.containsKey("surname") && requestParams.get("surname").length()>0){
                name += " "+requestParams.get("surname");
            }
            Account account = new Account("Coordinator", 0, name);
            Coordinator coordinator = new Coordinator(requestParams.get("associatedCharity"), account);
            cri.addNewCoordinator(coordinator);

            model.addAttribute(coordinator);
            return "adminAddCoordinatorAccountFormSuccess";

        }
        return "adminAddCoordinatorAccountFormFailed";
    }

}