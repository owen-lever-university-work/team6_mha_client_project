package com.nsa.team6.clientproject.controllers;

import com.nsa.team6.clientproject.forms.PaymentTransactionForm;
import com.nsa.team6.clientproject.services.AccountService;
import com.nsa.team6.clientproject.services.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping(path = "/admin")
public class AdminPaymentController {

    static final Logger LOG = LoggerFactory.getLogger(AdminPaymentController.class);

    private AccountService accountService;
    private TransactionService transactionService;

    @Autowired
    public AdminPaymentController(AccountService aService, TransactionService tService){
        accountService = aService;
        transactionService = tService;
    }

    @RequestMapping(value = "/adminPayment", method = RequestMethod.GET)
    public ModelAndView showForm(Model model) {
        model.addAttribute("accounts", accountService.findByAccountType("Coordinator"));
        model.addAttribute("paymentForm", new PaymentTransactionForm());
        return new ModelAndView("adminPaymentForm");
    }

    @RequestMapping(value = "/adminPayment", method = RequestMethod.POST)
    public String createTransactionFromForm(Model model, @ModelAttribute("PaymentTransactionForm") @Valid PaymentTransactionForm paymentForm, BindingResult bindingResult) {
        //If there are errors with the inputted form info, then the form page will be returned again
        if (bindingResult.hasErrors()) {
            LOG.error(bindingResult.toString());
            LOG.error("Form has binding errors");
            return "adminPaymentForm";
        }
        System.out.println("Form received");

        //Converts the form inputs into the payment transaction
        //TODO Change debit account so it is not hardcoded
        transactionService.paymentTransaction(paymentForm.getNumberOfTokens(), paymentForm.getWhoCreated(), paymentForm.getCreditAccountId(), 1);
        //TODO: Return transaction receipt
        return "adminSuccessAddedTokens";
    }
}
