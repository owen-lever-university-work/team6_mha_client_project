package com.nsa.team6.clientproject.controllers.exceptions;


public class MissingResourceException extends RuntimeException {

    protected String returnUrl;

    public MissingResourceException(String msg, String aUrl) {
        super(msg);
        returnUrl = aUrl;
    }

    public MissingResourceException(String msg) {
        this(msg, "/");
    }

    public MissingResourceException() {
        this("Missing Resource", "/");
    }
}