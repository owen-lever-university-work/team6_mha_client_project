package com.nsa.team6.clientproject.controllers;

import com.nsa.team6.clientproject.domain.Account;
import com.nsa.team6.clientproject.forms.AddTokenForm;
import com.nsa.team6.clientproject.services.AccountService;
import com.nsa.team6.clientproject.services.TokenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Controller
@RequestMapping(path = "/admin")
public class AdminDashboardController {

    static final Logger LOG = LoggerFactory.getLogger(AdminAddTokensController.class);

    private AccountService accountService;

    @Autowired
    public AdminDashboardController(AccountService aService){
        accountService = aService;
    }

    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public ModelAndView showForm(Model model) {
        Iterable<Account> iterable = accountService.findByAccountType("Admin");
        List<Account> accounts = StreamSupport
                .stream(iterable.spliterator(), false)
                .collect(Collectors.toList());

        model.addAttribute("accountBalance", accounts.get(0).getBalance());
        model.addAttribute("volunteerCount", accountService.getCountByAccountType("Volunteer"));
        model.addAttribute("coordinatorCount", accountService.getCountByAccountType("Coordinator"));
        model.addAttribute("businessCount", accountService.getCountByAccountType("Business"));
        return new ModelAndView("adminDashboard");
    }
}
