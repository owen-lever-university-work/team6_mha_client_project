package com.nsa.team6.clientproject.controllers;

import com.nsa.team6.clientproject.domain.Account;
import com.nsa.team6.clientproject.domain.Transaction;
import com.nsa.team6.clientproject.services.AccountService;
import com.nsa.team6.clientproject.services.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping(path = "/admin")
public class AdminTransactionListController {
    static final Logger LOG = LoggerFactory.getLogger(AdminAddTokensController.class);

    private AccountService accountService;
    private TransactionService transactionService;

    @Autowired
    public AdminTransactionListController(AccountService aService, TransactionService tService){
        accountService = aService;
        transactionService = tService;
    }

    @RequestMapping(value = "/transactionList", method = RequestMethod.GET)
    public ModelAndView showForm(Model model) {
        List<Transaction> transactionList = (List<Transaction>) transactionService.allTransactionsByDate();
        model.addAttribute("transactions", transactionList);
        return new ModelAndView("adminTransactionList");
    }
}
