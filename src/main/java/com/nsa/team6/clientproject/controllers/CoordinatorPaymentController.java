package com.nsa.team6.clientproject.controllers;

import com.nsa.team6.clientproject.domain.Account;
import com.nsa.team6.clientproject.forms.PaymentTransactionForm;
import com.nsa.team6.clientproject.services.AccountService;
import com.nsa.team6.clientproject.services.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping(path = "/coordinator")
public class CoordinatorPaymentController {
    static final Logger LOG = LoggerFactory.getLogger(AdminPaymentController.class);

    private AccountService accountService;
    private TransactionService transactionService;

    @Autowired
    public CoordinatorPaymentController(AccountService aService, TransactionService tService){
        accountService = aService;
        transactionService = tService;
    }

    @RequestMapping(value = "/payment", method = RequestMethod.GET)
    public ModelAndView showForm(Model model) {
        model.addAttribute("accounts", accountService.findByAccountType("Volunteer"));
        model.addAttribute("paymentForm", new PaymentTransactionForm());
        return new ModelAndView("coordinatorPayForm.html");
    }

    @RequestMapping(value = "/payment", method = RequestMethod.POST)
    public String createTransactionFromForm(Model model, @ModelAttribute("PaymentTransactionForm") @Valid PaymentTransactionForm paymentForm, BindingResult bindingResult) {
        //If there are errors with the inputted form info, then the form page will be returned again
        if (bindingResult.hasErrors()) {
            LOG.error(bindingResult.toString());
            LOG.error("Form has binding errors");
            return "coordinatorPayForm.html";
        }
        LOG.info("Form received");
        System.out.println("Num tokens: " + paymentForm.getNumberOfTokens() +  paymentForm.getWhoCreated()+  " Credit: " + paymentForm.getCreditAccountId()+ " Debit: " + paymentForm.getDebitAccountId());

        //Converts the form inputs into the payment transaction
        transactionService.paymentTransaction(paymentForm.getNumberOfTokens(), paymentForm.getWhoCreated(), paymentForm.getCreditAccountId(), paymentForm.getDebitAccountId());
        String accountName = accountService.findById(paymentForm.getCreditAccountId()).get().getAccountName();
        int yourTotal = accountService.findById(paymentForm.getDebitAccountId()).get().getBalance();

        //Adding attributes for transaction script
        model.addAttribute("payedTo", accountName);
        model.addAttribute("payedAmount", paymentForm.getNumberOfTokens());
        model.addAttribute("yourTotal", yourTotal);
        return "/coordinatorSuccessPayment";
    }
}
