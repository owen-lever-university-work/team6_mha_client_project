package com.nsa.team6.clientproject.controllers;

import com.nsa.team6.clientproject.controllers.exceptions.MissingResourceException;
import com.nsa.team6.clientproject.domain.Account;
import com.nsa.team6.clientproject.domain.Volunteer;
import com.nsa.team6.clientproject.services.AccountService;
import com.nsa.team6.clientproject.services.TransactionService;
import com.nsa.team6.clientproject.services.VolRegServiceStatic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Optional;

@Controller
@RequestMapping(path = "/admin")
public class AdminVolunteerProfileController {

    private AccountService accountService;
    private TransactionService transactionService;
    private VolRegServiceStatic volRegServiceStatic;

    @Autowired
    public AdminVolunteerProfileController(AccountService aService){
        accountService = aService;
    }

    @RequestMapping(path = "/volunteerAccount/{id}", method = RequestMethod.GET)
    public String getVolunteerProfile(@PathVariable int id, Model model) {
        Optional<Account> account = accountService.findById(id);
        if (account.isPresent()) {
            System.out.println(account.get());
            //Optional<Volunteer> volunteer = volRegServiceStatic.findByAccountId(id);
            //volunteer.ifPresent(volunteer1 -> transactionService.dateTransactionStatement(volunteer1.getId()));
            //model.addAttribute("statement", );
            model.addAttribute("chosenAccount", account.get());
            return "adminVolunteerProfile";
        } else {
            throw new MissingResourceException("No matching charity", "404");
        }
    }
}
