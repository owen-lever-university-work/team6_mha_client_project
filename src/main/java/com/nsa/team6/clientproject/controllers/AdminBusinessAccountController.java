package com.nsa.team6.clientproject.controllers;

import com.nsa.team6.clientproject.domain.Business;
import com.nsa.team6.clientproject.services.BusinessAccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class AdminBusinessAccountController {
    private BusinessAccountService businessAccountService;

    static final Logger LOG = LoggerFactory.getLogger(AdminBusinessAccountController.class);

    @Autowired
    public AdminBusinessAccountController(BusinessAccountService aService) {
        businessAccountService = aService;
    }

    //Handler request method to GET the form up on local host
    @RequestMapping(path = "/admin/Add_Business", method = RequestMethod.GET)
    public String addBusinessAccount(Model model) {
        LOG.debug("Handling GET request to /addBusinessAccount");
        //Returns the add account page
        return "addBusinessAccount";

    }
    //Handler request method to POST the data on the form sending it to the repo
    @RequestMapping(path = "/admin/Add_Business", method = RequestMethod.POST)
    public String addBusinessAccount(Business business, Model model) {

        LOG.debug(business.toString());

        businessAccountService.addBusinessAccount(business);

        model.addAttribute("business", business);

        return "addBusinessAccount";

    }
}


