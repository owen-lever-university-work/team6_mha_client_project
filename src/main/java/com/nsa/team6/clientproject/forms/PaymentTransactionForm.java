package com.nsa.team6.clientproject.forms;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PaymentTransactionForm {

    @NotNull
    private int numberOfTokens;

    @NotNull
    private String whoCreated;

    @NotNull
    private int creditAccountId;

    @NotNull
    private int debitAccountId;
}
