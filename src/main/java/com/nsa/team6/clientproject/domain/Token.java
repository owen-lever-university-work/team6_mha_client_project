package com.nsa.team6.clientproject.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Token {

    private int id;

    private Timestamp createdOn;

    private int accountId;
}
