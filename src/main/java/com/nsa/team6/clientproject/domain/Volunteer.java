package com.nsa.team6.clientproject.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor

//volunteer domain class
public class Volunteer {

    //fields based on the form given by MHA
    private int id;

    private Date registrationDate;

    private String volunteerName;

    private String volunteerAddress;

    private String volunteerPostCode;

    private String volunteerPhoneNo;

    private String emergencyContactNo;

    private String volunteerEmail;

    private Boolean isMHATenant;

    private Boolean isVolunteer;

    private Boolean isDisabilities;

    private Boolean isMedicalConditions;

    private Long volunteerAllergies;

    //constructor
//    public Volunteer (int id, Date registrationDate, String volunteerName, String volunteerAddress, String volunteerPostCode, String volunteerPhoneNo,
//                      String emergencyContactNo, String volunteerEmail, Boolean isMHATenant, Boolean isVolunteer, Boolean isDisabilities,
//                      Boolean isMedicalConditions, Long volunteerAllergies) {
//
//        this.id = id;
//        this.registrationDate = registrationDate;
//        this.volunteerName = volunteerName;
//        this.volunteerAddress = volunteerAddress;
//        this.volunteerPostCode = volunteerPostCode;
//        this.volunteerPhoneNo = volunteerPhoneNo;
//        this.emergencyContactNo = emergencyContactNo;
//        this.volunteerEmail = volunteerEmail;
//        this.isMHATenant = isMHATenant;
//        this.isVolunteer = isVolunteer;
//        this.isDisabilities = isDisabilities;
//        this.isMedicalConditions = isMedicalConditions;
//        this.volunteerAllergies = volunteerAllergies;
//
//    }

    //accessors
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public Date getRegistrationDate() {
//        return registrationDate;
//    }
//
//    public void setRegistrationDate(Date registrationDate) {
//        this.registrationDate = registrationDate;
//    }
//
//    public String getVolunteerName() {
//        return volunteerName;
//    }
//
//    public void setVolunteerName(String volunteerName) {
//        this.volunteerName = volunteerName;
//    }
//
//    public Long getVolunteerAddress() {
//        return volunteerAddress;
//    }
//
//    public void setVolunteerAddress(Long volunteerAddress) {
//        this.volunteerAddress = volunteerAddress;
//    }
//
//    public char getVolunteerPostCode() {
//        return volunteerPostCode;
//    }
//
//    public void setVolunteerPostCode(char volunteerPostCode) {
//        this.volunteerPostCode = volunteerPostCode;
//    }
//
//    public int getVolunteerPhoneNo() {
//        return volunteerPhoneNo;
//    }
//
//    public void setVolunteerPhoneNo(int volunteerPhoneNo) {
//        this.volunteerPhoneNo = volunteerPhoneNo;
//    }
//
//    public int getEmergencyContactNo() {
//        return emergencyContactNo;
//    }
//
//    public void setEmergencyContactNo(int emergencyContactNo) {
//        this.emergencyContactNo = emergencyContactNo;
//    }
//
//    public Long getVolunteerEmail() {
//        return volunteerEmail;
//    }
//
//    public void setVolunteerEmail(Long volunteerEmail) {
//        this.volunteerEmail = volunteerEmail;
//    }
//
//    public Boolean getMHATenant() {
//        return isMHATenant;
//    }
//
//    public void setMHATenant(Boolean MHATenant) {
//        isMHATenant = MHATenant;
//    }
//
//    public Boolean getVolunteer() {
//        return isVolunteer;
//    }
//
//    public void setVolunteer(Boolean volunteer) {
//        isVolunteer = volunteer;
//    }
//
//    public Boolean getDisabilities() {
//        return isDisabilities;
//    }
//
//    public void setDisabilities(Boolean disabilities) {
//        isDisabilities = disabilities;
//    }
//
//    public Boolean getMedicalConditions() {
//        return isMedicalConditions;
//    }
//
//    public void setMedicalConditions(Boolean medicalConditions) {
//        isMedicalConditions = medicalConditions;
//    }
//
//    public Long getVolunteerAllergies() {
//        return volunteerAllergies;
//    }
//
//    public void setVolunteerAllergies(Long volunteerAllergies) {
//        this.volunteerAllergies = volunteerAllergies;
//    }
}
