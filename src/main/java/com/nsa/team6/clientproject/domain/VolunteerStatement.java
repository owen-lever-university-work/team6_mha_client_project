package com.nsa.team6.clientproject.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VolunteerStatement {

    private int id;

    private Date dateOfTransaction;

    private int tokensPaid;

    private int balance;
}
