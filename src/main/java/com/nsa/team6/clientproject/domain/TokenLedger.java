package com.nsa.team6.clientproject.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TokenLedger {

    private int id;

    private String direction;

    private Timestamp createdOn;

    private int accountId;

    private int transactionId;

    private int tokenId;
}
