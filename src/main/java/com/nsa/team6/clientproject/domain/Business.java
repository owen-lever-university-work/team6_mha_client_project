package com.nsa.team6.clientproject.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


//domain class for business
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Business {
    //fields in business table
    private int id;
    private String businessName;
    private String businessAddress;
    private String businessPostCode;
    private String businessPhoneNo;
    private String businessEmail;
    private int account_id;
}

