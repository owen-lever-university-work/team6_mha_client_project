package com.nsa.team6.clientproject.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Account {

    private int id;

    private String accountType;

    private int balance;

    private String accountName;

    public Account(String accountType, int balance, String accountName) {
        this.accountType = accountType;
        this.balance = balance;
        this.accountName = accountName;
    }
}
