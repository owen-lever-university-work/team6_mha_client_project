package com.nsa.team6.clientproject.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Coordinator {
    //Copy and paste annotation from Account domain to here
    //Add relevant fields to this class

    private long id;

    private String associatedCharity;

    private Account account;

    public Coordinator(String associatedCharity, Account account) {
        this.associatedCharity = associatedCharity;
        this.account = account;
    }
}
