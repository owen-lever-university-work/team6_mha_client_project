package com.nsa.team6.clientproject.services;

import com.nsa.team6.clientproject.domain.Business;
import com.nsa.team6.clientproject.repositories.BusinessRepoInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
//
@Service
public class BusinessAccountStaticService implements BusinessAccountService{

    private BusinessRepoInterface businessRepoInterface;
    //service connects static to repo and save business
    @Autowired
    public BusinessAccountStaticService(BusinessRepoInterface aCharBusinessRepoInterface) { businessRepoInterface = aCharBusinessRepoInterface;
    }

    @Override
    @Transactional(readOnly = false)
    public void addBusinessAccount(Business business) { businessRepoInterface.save(business);

    }
}

