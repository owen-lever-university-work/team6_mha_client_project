package com.nsa.team6.clientproject.services;

import com.nsa.team6.clientproject.domain.Token;

public interface TokenService {

    void addTokensToAccount(int numTokens, String whoCreated, int accountId);

    Iterable<Token> findByAccountId(int id);
}
