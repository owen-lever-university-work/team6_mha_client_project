package com.nsa.team6.clientproject.services;

import com.nsa.team6.clientproject.domain.Volunteer;
import com.nsa.team6.clientproject.repositories.VolunteerRegisterInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class VolRegServiceStatic implements VolunteerRegisterService {

//    private List<Volunteer> volunteerList;

    private VolunteerRegisterInterface volunteerRegisterInterface;

    @Autowired
    public VolRegServiceStatic (VolunteerRegisterInterface aCharVolunteerRepoInterface) {

        volunteerRegisterInterface = aCharVolunteerRepoInterface;

    }

//    @Override
//    public List <Volunteer> findAll() { return (List<Volunteer>) volunteerRegisterInterface.findAll(); }

    @Override
    @Transactional(readOnly = false)
    public void addVolunteer (Volunteer volunteer) { volunteerRegisterInterface.save(volunteer);}

    @Override
    public Optional<Volunteer> findByAccountId(int accountId) {
        return volunteerRegisterInterface.findByAccountId(accountId);
    }

}
