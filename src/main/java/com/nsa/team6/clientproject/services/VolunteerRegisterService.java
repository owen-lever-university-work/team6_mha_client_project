package com.nsa.team6.clientproject.services;

import com.nsa.team6.clientproject.domain.Volunteer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


public interface VolunteerRegisterService {

//    public List<Volunteer> findAll();


    public void addVolunteer (Volunteer volunteer);

    Optional<Volunteer> findByAccountId(int id);

}
