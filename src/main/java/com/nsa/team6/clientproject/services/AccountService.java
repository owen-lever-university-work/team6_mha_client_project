package com.nsa.team6.clientproject.services;

import com.nsa.team6.clientproject.domain.Account;

import java.util.Optional;

public interface AccountService {

        Iterable<Account> findByAccountType(String type);

        Optional<Account> findById(int id);

        String getCountByAccountType(String type);
}
