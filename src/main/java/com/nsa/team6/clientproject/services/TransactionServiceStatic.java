package com.nsa.team6.clientproject.services;

import com.nsa.team6.clientproject.domain.Transaction;
import com.nsa.team6.clientproject.repositories.TransactionRepoInterface;
import org.springframework.stereotype.Service;

@Service
public class TransactionServiceStatic  implements TransactionService{

    private TransactionRepoInterface transactionRepo;

    public TransactionServiceStatic(TransactionRepoInterface aRepo){
        transactionRepo = aRepo;
    }

    @Override
    public void paymentTransaction(int numTokens, String whoCreated, int creditId, int debitId) {
        transactionRepo.paymentTransaction(numTokens, whoCreated, creditId, debitId);
    }

    @Override
    public Iterable<Transaction> allTransactionsByDate() {
        return transactionRepo.findAll();
    }

    @Override
    public void dateTransactionStatement(int volunteerId) {
       transactionRepo.dateTransactionStatement(volunteerId);
    }
}
