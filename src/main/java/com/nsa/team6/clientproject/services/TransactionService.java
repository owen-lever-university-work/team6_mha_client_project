package com.nsa.team6.clientproject.services;

import com.nsa.team6.clientproject.domain.Transaction;

public interface TransactionService {

    void paymentTransaction(int numTokens, String whoCreated, int creditId, int debitId);

    Iterable<Transaction> allTransactionsByDate();

    void dateTransactionStatement(int volunteerId);
}
