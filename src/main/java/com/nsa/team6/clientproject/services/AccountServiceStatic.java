package com.nsa.team6.clientproject.services;

import com.nsa.team6.clientproject.domain.Account;
import com.nsa.team6.clientproject.repositories.AccountRepoInterface;
import com.nsa.team6.clientproject.repositories.TokenRepoInterface;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AccountServiceStatic implements AccountService{

    private AccountRepoInterface accountRepo;

    public AccountServiceStatic(AccountRepoInterface aRepo){
        accountRepo = aRepo;
    }

    public Iterable<Account> findByAccountType(String type){
        return accountRepo.findByAccountType(type);
    }

    public Optional<Account> findById(int id){
        return accountRepo.findById(id);
    }

    public String getCountByAccountType(String type){
        return accountRepo.getCountByAccountType(type);
    }
}
