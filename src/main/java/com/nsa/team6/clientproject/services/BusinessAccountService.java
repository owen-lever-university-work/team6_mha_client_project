package com.nsa.team6.clientproject.services;

import com.nsa.team6.clientproject.domain.Business;

public interface BusinessAccountService {
    public void addBusinessAccount(Business business);
}

