package com.nsa.team6.clientproject.services;

import com.nsa.team6.clientproject.domain.Token;
import com.nsa.team6.clientproject.repositories.TokenRepoInterface;
import org.springframework.stereotype.Service;

@Service
public class TokenServiceStatic implements TokenService {

    private TokenRepoInterface tokenRepo;

    public TokenServiceStatic(TokenRepoInterface aRepo){
        tokenRepo = aRepo;
    }

    public Iterable<Token> findByAccountId(int id){
        return tokenRepo.findByAccountId(id);
    }

    public void addTokensToAccount(int numTokens, String whoCreated, int ownerAccountId){tokenRepo.addTokensToAccount(numTokens, whoCreated, ownerAccountId);}
}
