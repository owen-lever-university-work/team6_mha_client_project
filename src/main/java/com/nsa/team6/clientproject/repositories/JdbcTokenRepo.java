package com.nsa.team6.clientproject.repositories;

import com.nsa.team6.clientproject.domain.Token;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import javax.xml.crypto.Data;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Optional;

@Repository
public class JdbcTokenRepo implements TokenRepoInterface{

    static final Logger LOG = LoggerFactory.getLogger(JdbcTokenRepo.class);

    private JdbcTemplate jdbc;
    private DataSource dataSource;

    @Value("SELECT * FROM tokens")
    private String allTokens;

    @Value("SELECT * FROM tokens WHERE account_id = ?")
    private String byAccountId;

    @Value("CALL create_tokens(?, ?, ?)")
    private String addTokensAccount;

    @Autowired
    public JdbcTokenRepo(DataSource dataSource, JdbcTemplate jdbc) {
        this.jdbc = jdbc;
        this.dataSource = dataSource;
    }

    @Override
    public Iterable<Token> findAll() {
        return jdbc.query(allTokens,
                this::mapRowToToken);
    }

    @Override
    public Iterable<Token> findByAccountId(int id){
        return jdbc.query(byAccountId, this::mapRowToToken, id);
    }

    @Override
    public void addTokensToAccount(int numTokens, String whoCreated, int ownerAccountId){
        LOG.debug("OwnerAccountId " + ownerAccountId);
        if(ownerAccountId < 1){
            throw new IllegalArgumentException("Owner account id must be specified");
        }else {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("create_tokens");
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("numberOfTokens", numTokens)
                    .addValue("whoCreated", whoCreated)
                    .addValue("ownerAccountId", ownerAccountId);
            jdbcCall.execute(in);
        }
    }

    private Token mapRowToToken(ResultSet rs, int rowNum) throws SQLException {
        return new Token(rs.getInt("id"),
                rs.getTimestamp("created_on"),
                rs.getInt("account_id")
        );
    }
}
