package com.nsa.team6.clientproject.repositories;

import com.nsa.team6.clientproject.domain.TokenLedger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class JdbcTokenLedgerRepo implements TokenLedgerRepoInterface{

    static final Logger LOG = LoggerFactory.getLogger(JdbcTokenLedgerRepo.class);

    private JdbcTemplate jdbc;

    @Value("SELECT * FROM tokenledgers")
    private String allTokenLedgers;

    @Autowired
    public JdbcTokenLedgerRepo(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public Iterable<TokenLedger> findAll() {
        return jdbc.query(allTokenLedgers,
                this::mapRowToTokenLedger);
    }

    private TokenLedger mapRowToTokenLedger(ResultSet rs, int rowNum) throws SQLException {
        return new TokenLedger(rs.getInt("id"),
                rs.getString("direction"),
                rs.getTimestamp("created_on"),
                rs.getInt("account_id"),
                rs.getInt("transaction_id"),
                rs.getInt("token_id")
        );
    }
}
