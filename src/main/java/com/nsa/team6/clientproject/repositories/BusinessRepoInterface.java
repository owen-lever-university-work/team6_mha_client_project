package com.nsa.team6.clientproject.repositories;

import com.nsa.team6.clientproject.domain.Business;

public interface BusinessRepoInterface {
    public void save(Business business);
}
