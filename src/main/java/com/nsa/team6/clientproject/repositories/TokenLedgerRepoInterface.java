package com.nsa.team6.clientproject.repositories;

import com.nsa.team6.clientproject.domain.TokenLedger;

public interface TokenLedgerRepoInterface {

    Iterable<TokenLedger> findAll();
}
