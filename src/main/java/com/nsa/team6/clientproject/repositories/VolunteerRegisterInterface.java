package com.nsa.team6.clientproject.repositories;

import com.nsa.team6.clientproject.domain.Volunteer;

import java.util.List;
import java.util.Optional;

public interface VolunteerRegisterInterface {

//    Iterable<Volunteer> findAll();

    public Volunteer save (Volunteer volunteer);

    Optional<Volunteer> findByAccountId(int id);

}
