package com.nsa.team6.clientproject.repositories;

import com.nsa.team6.clientproject.domain.Transaction;

import java.sql.SQLException;

public interface TransactionRepoInterface {

    Iterable<Transaction> findAll();

    void deleteById(int id);

    void paymentTransaction(int numTokens, String whoCreated,int creditAccountId, int debitAccountId);

    void dateTransactionStatement(int volunteerId);
}
