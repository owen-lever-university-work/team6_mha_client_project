package com.nsa.team6.clientproject.repositories;

import com.nsa.team6.clientproject.domain.Account;
import com.nsa.team6.clientproject.domain.Coordinator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

@Repository
public class JdbcCoordinatorRepo implements CoordinatorRepoInterface{

    static final Logger LOG = LoggerFactory.getLogger(JdbcAccountRepo.class);

    private JdbcTemplate jdbc;
    private AccountRepoInterface accountRepo;

    @Value("SELECT * FROM coordinators")
    private String allCoordinators;

    @Value("CALL create_coordinator_accounts(?, ?, ?)")
    private String addCordinator;


    @Autowired
    public JdbcCoordinatorRepo(JdbcTemplate jdbc, AccountRepoInterface accountRepo)
    {
        this.jdbc = jdbc;
        this.accountRepo = accountRepo;
    }


    @Override
    public Iterable<Coordinator> findAll() {
        return jdbc.query(allCoordinators,
                this::mapRowToCoordinator);
    }

    @Override
    public Optional<Coordinator> findById(int id) {
        return Optional.empty();
    }

    @Override
    public Coordinator addNewCoordinator(Coordinator coordinator) {
        int balance = coordinator.getAccount().getBalance();
        String accountName = coordinator.getAccount().getAccountName();
        String associatedCharity = coordinator.getAssociatedCharity();
        jdbc.update(addCordinator, balance, accountName, associatedCharity);
        return coordinator;
    }


    private Coordinator mapRowToCoordinator(ResultSet rs, int rowNum) throws SQLException{
        long id = rs.getLong("id");
        String associatedCharity = rs.getString("associated_charity");
        long accountId = rs.getLong("account_id");
        Coordinator coordinator;
        //TODO: Refactor the code so that type is int not long and to use findById instead of findByIdLong
        Optional<Account> account = accountRepo.findByIdLong(accountId);
        coordinator = new Coordinator(accountId, associatedCharity, account.get());
        return coordinator;
    }
}
