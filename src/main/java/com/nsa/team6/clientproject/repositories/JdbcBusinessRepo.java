package com.nsa.team6.clientproject.repositories;


import com.nsa.team6.clientproject.domain.Business;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

@Repository
public class JdbcBusinessRepo implements BusinessRepoInterface {

    static final Logger LOG = LoggerFactory.getLogger(JdbcBusinessRepo.class);

    @Autowired
    private JdbcTemplate jdbc;
    //calling the stored procedure
    @Value("CALL create_business_account(?,?,?,?,?)")
    private String businessInsertSQL;

    //method inserts data into the business table
    @Override
    public void save(Business business) {
        jdbc.update(businessInsertSQL,
                business.getBusinessName(),
                business.getBusinessAddress(),
                business.getBusinessPostCode(),
                business.getBusinessPhoneNo(),
                business.getBusinessEmail());

    }

    }

