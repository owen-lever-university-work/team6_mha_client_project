package com.nsa.team6.clientproject.repositories;

import com.nsa.team6.clientproject.domain.Coordinator;

import java.util.Optional;

public interface CoordinatorRepoInterface {

    Iterable<Coordinator> findAll();

    Optional<Coordinator> findById(int id);

    Coordinator addNewCoordinator(Coordinator coordinator);
}
