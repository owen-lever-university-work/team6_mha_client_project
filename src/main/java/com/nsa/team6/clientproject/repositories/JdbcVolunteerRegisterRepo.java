package com.nsa.team6.clientproject.repositories;

import com.nsa.team6.clientproject.domain.Volunteer;
import lombok.Generated;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.Optional;

@Repository
public class JdbcVolunteerRegisterRepo implements VolunteerRegisterInterface{

    static final Logger LOG = LoggerFactory.getLogger(JdbcVolunteerRegisterRepo.class);

    private JdbcTemplate jdbc;

    //@Value("SELECT * FROM volunteers")
//    private String allVolunteers;

    @Autowired
    public JdbcVolunteerRegisterRepo (JdbcTemplate jdbc) {this.jdbc = jdbc;}

    @Value("CALL create_volunteer_accounts(?,?,?,?,?,?,?,?,?,?,?,?)")
    private String volunteerInsertSQL;

    @Value("SELECT * FROM  volunteers WHERE account_id = ?")
    private String getByAccountId;

//    @Override
//    public Iterable<Volunteer> findAll() {
//        return null;
//    }

    @Override
    public Optional<Volunteer> findByAccountId(int id) {
        return Optional.of(
                jdbc.queryForObject(
                        getByAccountId, this::mapRowToVolunteer, id)
        );
    }

    @Override
    public Volunteer save(Volunteer volunteer) {
        jdbc.update(volunteerInsertSQL,
                volunteer.getRegistrationDate(),
                volunteer.getVolunteerName(),
                volunteer.getVolunteerAddress(),
                volunteer.getVolunteerPostCode(),
                volunteer.getVolunteerPhoneNo(),
                volunteer.getEmergencyContactNo(),
                volunteer.getVolunteerEmail(),
                volunteer.getIsMHATenant(),
                volunteer.getIsVolunteer(),
                volunteer.getIsDisabilities(),
                volunteer.getIsMedicalConditions(),
                volunteer.getVolunteerAllergies());

        return volunteer;
    }

//    @Override
//    public Iterable<Volunteer> findAll() {
//        return jdbc.query(allVolunteers,
//                this::mapRowToVolunteer);
//    }

//    @Override
//    public Void save(Volunteer volunteer){
//        GeneratedKeyHolder holder = new GeneratedKeyHolder();
//        jdbc.update(
//                new PreparedStatementCreator() {
//                    @Override
//                    public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
//                        PreparedStatement ps =
//                                con.prepareStatement(allVolunteers, Statement.RETURN_GENERATED_KEYS);
//                        ps.setString(1,volunteer.getVolunteerName());
//                        ps.setString(2, volunteer.getVolunteerAddress());
//                        ps.setString(3, volunteer.getVolunteerPostCode());
//                        ps.setString(4, volunteer.getVolunteerPhoneNo());
//                        ps.setInt(5, volunteer.getEmergencyContactNo());
//                        ps.setString(6, volunteer.getVolunteerEmail());
//                        ps.setBoolean(7, volunteer.getIsMHATenant());
//                        ps.setBoolean(8, volunteer.getIsVolunteer());
//                        ps.setBoolean(9, volunteer.getIsDisabilities());
//                        ps.setBoolean(10, volunteer.getIsMedicalConditions());
//                        ps.setLong(11, volunteer.getVolunteerAllergies());
//                        return ps;
//                    }
//                },
//                holder);
//        volunteer.setId(holder.getKey().intValue());
//        LOG.debug(volunteer.toString());
//        return save(volunteer);
//    }

    private Volunteer mapRowToVolunteer(ResultSet rs, int rowNum) throws SQLException{
        return new Volunteer(rs.getInt("id"),
                rs.getDate("registration_date"),
                rs.getString("volunteer_name"),
                rs.getString("address"),
                rs.getString("postcode"),
                rs.getString("phone_number"),
                rs.getString("emergency_number"),
                rs.getString("email"),
                rs.getBoolean("tenancy"),
                rs.getBoolean("volunteered"),
                rs.getBoolean("disabilities"),
                rs.getBoolean("medical_conditions"),
                rs.getLong("allergies"));
    }
}
