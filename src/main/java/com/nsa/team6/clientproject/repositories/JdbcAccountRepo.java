package com.nsa.team6.clientproject.repositories;


import com.nsa.team6.clientproject.domain.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public class JdbcAccountRepo implements AccountRepoInterface{

    static final Logger LOG = LoggerFactory.getLogger(JdbcAccountRepo.class);

    private JdbcTemplate jdbc;
    private DataSource dataSource;

    @Value("SELECT * FROM accounts")
    private String allAccounts;

    @Value("SELECT * FROM accounts WHERE account_type = ?")
    private String byAccountType;

    @Value("SELECT * FROM accounts WHERE id = ?")
    private String byId;

    @Value("SELECT count_account_types(?)")
    private String countAccountType;

    @Autowired
    public JdbcAccountRepo(JdbcTemplate jdbc, DataSource dataSource) {
        this.jdbc = jdbc;
        this.dataSource = dataSource;
    }

    @Override
    public Iterable<Account> findAll() {
        return jdbc.query(allAccounts,
                this::mapRowToAccount);
    }

    @Override
    public Iterable<Account> findByAccountType(String type) {
        return jdbc.query(byAccountType, this::mapRowToAccount, type);
    }

    @Override
    public Optional<Account> findById(int id) {
        return Optional.of(
                jdbc.queryForObject(
                        byId, this::mapRowToAccount, id)
        );
    }

    @Override
    public Optional<Account> findByIdLong(long id) {
        List<Account> account = jdbc.query(allAccounts+" WHERE id = "+id, this::mapRowToAccount);
        LOG.debug(account.size()+" : SIZE");
        if(account.size() < 1){
            return Optional.empty();
        }else{
            return Optional.of(account.get(0));
        }
    }

    public String getCountByAccountType(String type){
        List<String> list = jdbc.query(countAccountType, this::mapRowToOneString, type);
        System.out.println(list);
        return list.get(0);
    }

    private String mapRowToOneString(ResultSet rs, int rowNum) throws SQLException{
        return rs.getString(1);
    }

    private Account mapRowToAccount(ResultSet rs, int rowNum) throws SQLException{
        return new Account(rs.getInt("id"),
                rs.getString("account_type"),
                rs.getInt("balance"),
                rs.getString("account_name")
        );
    }
}
