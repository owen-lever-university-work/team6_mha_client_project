package com.nsa.team6.clientproject.repositories;

import com.nsa.team6.clientproject.domain.Account;

import java.util.Optional;

public interface AccountRepoInterface {

    Iterable<Account> findAll();

    Iterable<Account> findByAccountType(String type);
    
    Optional<Account> findById(int id);

    Optional<Account> findByIdLong(long id);

    String getCountByAccountType(String type);
}
