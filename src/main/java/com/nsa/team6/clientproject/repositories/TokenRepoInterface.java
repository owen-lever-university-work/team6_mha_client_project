package com.nsa.team6.clientproject.repositories;

import com.nsa.team6.clientproject.domain.Token;

import java.util.Optional;

public interface TokenRepoInterface {

    Iterable<Token> findAll();

   Iterable<Token> findByAccountId(int id);

    void addTokensToAccount(int numTokens, String whoCreated,int ownerAccountId);
}
