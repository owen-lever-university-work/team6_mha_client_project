package com.nsa.team6.clientproject.repositories;

import com.nsa.team6.clientproject.domain.Transaction;
import com.nsa.team6.clientproject.domain.VolunteerStatement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Map;

@Repository
public class JdbcTransactionRepo implements TransactionRepoInterface{

    static final Logger LOG = LoggerFactory.getLogger(JdbcTransactionRepo.class);

    private JdbcTemplate jdbc;
    private DataSource dataSource;

    @Value("SELECT * FROM transactions ORDER BY created_on DESC")
    private String allTransactions;

    @Value("DELETE FROM transactions WHERE id = ?")
    private String deleteById;

    @Autowired
    public JdbcTransactionRepo(DataSource dataSource, JdbcTemplate jdbc) {
        this.jdbc = jdbc;
        this.dataSource = dataSource;
    }

    @Override
    public Iterable<Transaction> findAll() {
        return jdbc.query(allTransactions,
                this::mapRowToTransaction);
    }

    @Override
    public void paymentTransaction(int numTokens, String whoCreated, int creditAccountId, int debitAccountId){
        LOG.debug("CreditAccountId " + creditAccountId);
        if(creditAccountId < 1 || debitAccountId < 1){
            throw new IllegalArgumentException("Credit and debit account id must be specified");
        }else {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("payment_transaction");
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("numberOfTokens", numTokens)
                    .addValue("whoCreated", whoCreated)
                    .addValue("creditAccountId", creditAccountId)
                    .addValue("debitAccountId", debitAccountId);
            System.out.println("IN STRING " + ((MapSqlParameterSource) in).getValues().toString());
            jdbcCall.execute(in);
        }
    }

    @Override
    public void dateTransactionStatement(int volunteerId) {
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("get_volunteer_statement");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("volunteerId", volunteerId);
        Map<String, Object> out = jdbcCall.execute(in);
        VolunteerStatement volunteerStatement  = new VolunteerStatement();
        volunteerStatement.setId((int) out.get("id"));
        volunteerStatement.setDateOfTransaction((Date) out.get("date_of_transaction"));
        volunteerStatement.setTokensPaid((int) out.get("tokens_paid"));
        volunteerStatement.setBalance((int) out.get("balance"));
        System.out.println(volunteerStatement);
    }


    @Override
    public void deleteById(int id) {
        jdbc.update(deleteById, id);
    }

    private Transaction mapRowToTransaction(ResultSet rs, int rowNum) throws SQLException {
        return new Transaction(rs.getInt("id"),
                rs.getTimestamp("created_on"),
                rs.getString("transaction_type"),
                rs.getString("created_by"),
                rs.getString("memo")
        );
    }
}
